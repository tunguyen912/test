
## Deployment

Step by step tutorial of how to deploy your application to Heroku

1. Login to docker
```bash
docker login --username=<heroku-account> --password=<heroku-api-key> registry.heroku.com
```

2. Build docker image
```bash
docker build -t registry.heroku.com/<heroku-app-name>/<dyno-type> .
```

3. Push docker image into Heroku registry
```bash
docker push registry.heroku.com/<heroku-app-name>/<dyno-type>
```

4. Release an image
```bash
heroku container:release <dyno-type> -a <heroku-app-name>
```

